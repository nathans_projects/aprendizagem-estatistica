from functools import lru_cache

@lru_cache(maxsize = 100)
def pascal_triangle(n, p):
    if p == 0:
        return 0
    elif p == 1:
        return n
    elif p == n:
        return 1
    else: return pascal_triangle(n-1, p-1) + pascal_triangle(n-1, p)

print(pascal_triangle(9,4))