#######################
### BUFFON'S NEEDLE ###
#######################
# Exemplo de Sullivan (2012) - Introduction to Data Mining for the Life Sciences


# Limpando a memória
# rm(list=ls(all=TRUE))


# definindo a função
piHat <- function(N){
  x <- runif(N)  # simulando N valores uniformes entre 0 e 1, atribuindo a x
  y <- runif(N)  # simulando N valores uniformes entre 0 e 1, atribuindo a y
  idx <- x^2 + y^2 <= 1  # verificando se os pontos caem dentro ou fora do círculo
  piHat <- length(which(idx))/N*4 # estimativa de pi
  plot(x,y, main = piHat)  # gráfico com todos os pontos simulados
  points(x[idx], y[idx], col = 'red')  # pintando de vermelho apenas os que caem dentro da circunferência
  sprintf('Estimate of pi = %.9f after %d iterations', piHat, N) # legenda
}

# simulando
par(mfrow=c(2,2))
for(i in 2:5){
  print(piHat(10^i))
}
